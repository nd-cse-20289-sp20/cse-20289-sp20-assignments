#!/bin/bash

SCRIPT=${1:-shell.py}
WORKSPACE=/tmp/$SCRIPT.$(id -u)
FAILURES=0

export LC_ALL=C

error() {
    echo "$@"
    echo
    [ -r $WORKSPACE/test ] && cat $WORKSPACE/test
    echo
    FAILURES=$((FAILURES + 1))
}

cleanup() {
    STATUS=${1:-$FAILURES}
    rm -fr $WORKSPACE
    exit $STATUS
}

input() {
    cat <<EOF
Toss a coin to your Witcher
O' Valley of Plenty
O' Valley of Plenty, oh
Toss a coin to Your Witcher
O' Valley of Plenty

At the edge of the world
Fight the mighty horde
That bashes and breaks you
And brings you to mourn
EOF
}

mkdir $WORKSPACE

trap "cleanup" EXIT
trap "cleanup 1" INT TERM

echo "Testing $SCRIPT ..."

printf " %-40s ... " "$SCRIPT"
diff -y <(input | ./$SCRIPT) <(input | head -n 5) &> $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "$SCRIPT -h"
input | ./$SCRIPT -h &> $WORKSPACE/test
if [ $? -eq 0 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "$SCRIPT -n 2"
diff -y <(input | ./$SCRIPT -n 2) <(input | head -n 2) &> $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "$SCRIPT -u"
diff -y <(input | ./$SCRIPT -u) <(input | head -n 5 | tr a-z A-Z) &> $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

printf " %-40s ... " "$SCRIPT -n 4 -u"
diff -y <(input | ./$SCRIPT -n 4 -u) <(input | head -n 4 | tr a-z A-Z) &> $WORKSPACE/test
if [ $? -ne 0 ]; then
    error "Failure"
else
    echo "Success"
fi

TESTS=$(($(grep -c Success $0) - 1))
echo "   Score $(echo "scale=2; ($TESTS - $FAILURES) / $TESTS.0 * 1.0" | bc | awk '{printf "%0.2f\n", $1}')"
echo
